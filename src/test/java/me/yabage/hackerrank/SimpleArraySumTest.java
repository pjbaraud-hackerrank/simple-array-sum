package me.yabage.hackerrank;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class SimpleArraySumTest {

    @Test
    public void sum_of_1_2_3_4_should_give_10(){
        assertThat(new SimpleArraySum().of(Arrays.asList(1,2,3,4)),is(equalTo(10)));
    }

    @Test
    public void sum_of_1_should_give_1(){
        assertThat(new SimpleArraySum().of(Collections.singletonList(1)),is(equalTo(1)));
    }

    @Test
    public void sum_of_empty_array_should_give_0(){
        assertThat(new SimpleArraySum().of(Collections.emptyList()),is(equalTo(0)));
    }
}
