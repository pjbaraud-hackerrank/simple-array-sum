package me.yabage.hackerrank.ui;

import me.yabage.hackerrank.SimpleArraySum;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.*;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CommandLineInterfaceTest {

    @Test
    public void commandline_should_print_10_when_entering_1_2_3_4() throws IOException {
        ConsoleReader consoleReader = Mockito.mock(ConsoleReader.class);
        ConsoleWriter consoleWriter = Mockito.mock(ConsoleWriter.class);
        SimpleArraySum sum = Mockito.mock(SimpleArraySum.class);
        when(consoleReader.readLine()).thenReturn("1 2 3 4");
        when(sum.of(anyList())).thenReturn(10);
        CommandLineInterface cli = new CommandLineInterface(consoleReader, consoleWriter, sum);
        cli.run();
        verify(consoleWriter).write("10");
    }

    @Test
    public void commandline_should_print_0_for_empty_array() throws IOException {
        ConsoleReader consoleReader = Mockito.mock(ConsoleReader.class);
        ConsoleWriter consoleWriter = Mockito.mock(ConsoleWriter.class);
        SimpleArraySum sum = Mockito.mock(SimpleArraySum.class);
        when(consoleReader.readLine()).thenReturn("");
        when(sum.of(anyList())).thenReturn(0);
        CommandLineInterface cli = new CommandLineInterface(consoleReader, consoleWriter, sum);
        cli.run();
        verify(consoleWriter).write("0");
    }
}
