package me.yabage.hackerrank.ui;

import java.util.Scanner;

/**
 * Created by now4n3 on 29/03/17.
 */
public class DefaultConsoleReader implements ConsoleReader{

    private Scanner scanner = new Scanner(System.in);

    @Override
    public String readLine() {
        return scanner.nextLine();
    }

}
