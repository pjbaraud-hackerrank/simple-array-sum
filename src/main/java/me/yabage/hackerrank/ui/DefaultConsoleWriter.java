package me.yabage.hackerrank.ui;

/**
 * Created by now4n3 on 29/03/17.
 */
public class DefaultConsoleWriter implements ConsoleWriter {

    @Override
    public void write(String line) {
        System.out.println(line);
    }
}
