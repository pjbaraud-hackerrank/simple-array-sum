package me.yabage.hackerrank.ui;

import me.yabage.hackerrank.SimpleArraySum;
import me.yabage.hackerrank.ui.ConsoleReader;
import me.yabage.hackerrank.ui.ConsoleWriter;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CommandLineInterface {

    private final ConsoleWriter consoleWriter;
    private final ConsoleReader consoleReader;
    private final SimpleArraySum sum;

    public CommandLineInterface(ConsoleReader consoleReader, ConsoleWriter consoleWriter, SimpleArraySum sum) {
        this.consoleReader = consoleReader;
        this.consoleWriter = consoleWriter;
        this.sum = sum;
    }

    public void run() {
        consoleReader.readLine();
        List<Integer> array = parseArray(consoleReader.readLine());
        Integer result = sum.of(array);
        consoleWriter.write(String.valueOf(result));
    }

    private List<Integer> parseArray(String arrayElements) {
        Scanner scanner = new Scanner(arrayElements);
        List<Integer> array = new ArrayList<>();
        while (scanner.hasNextInt()){
            array.add(scanner.nextInt());
        }
        scanner.close();
        return array;
    }

}
