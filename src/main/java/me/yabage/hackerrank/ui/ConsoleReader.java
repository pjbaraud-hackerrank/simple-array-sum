package me.yabage.hackerrank.ui;

/**
 * Created by now4n3 on 29/03/17.
 */
public interface ConsoleReader {

    String readLine();

}
