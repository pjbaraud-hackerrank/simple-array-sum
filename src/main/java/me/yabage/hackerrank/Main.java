package me.yabage.hackerrank;

import me.yabage.hackerrank.ui.CommandLineInterface;
import me.yabage.hackerrank.ui.DefaultConsoleReader;
import me.yabage.hackerrank.ui.DefaultConsoleWriter;

/**
 * Created by now4n3 on 29/03/17.
 */
public class Main {

    public static void main(String[] args) {
        CommandLineInterface cli = new CommandLineInterface(
                new DefaultConsoleReader(),
                new DefaultConsoleWriter(),
                new SimpleArraySum());
        cli.run();
    }
}
