package me.yabage.hackerrank;

import java.util.List;

public class SimpleArraySum {

    public Integer of(List<Integer> integers) {
        return integers.stream()
                .reduce(0, Integer::sum);
    }

}
